package com.bbva.qwai.batch.processor;

import org.springframework.batch.item.ItemProcessor;

public class Step2ItemProcessor implements ItemProcessor<String, String>{

	@Override
	public String process(String inputLine) throws Exception {
		String [] lineSplit = inputLine.split(",");
		String lineProcessedByStep = "";
		if (lineSplit.length == 5) {
			// Write only DNI, Nombre, Apellidos e email
			lineProcessedByStep = lineSplit[0] + "," + lineSplit[1] + "," + lineSplit[2] + "," + lineSplit[4];
		}
		else {
			// TODO: Manage errors on input file
		}
		return lineProcessedByStep;
	}
}
