package com.bbva.qwai.batch.processor;

import org.springframework.batch.item.ItemProcessor;

public class Step1ItemProcessor implements ItemProcessor<String, String>{

	@Override
	public String process(String inputLine) throws Exception {
		String [] lineSplit = inputLine.split(",");
		String lineProcessedByStep1 = "";
		if (lineSplit.length == 6) {
			// Only include "clients" (first field == Cliente) 
			if (lineSplit.equals("Cliente")) {
				lineProcessedByStep1 = lineSplit[1] + "," + lineSplit[2] + "," + lineSplit[3] + "," + lineSplit[4] + "," + lineSplit[5];
			}
		}
		else {
			// TODO: Manage errors on input file
		}
		return lineProcessedByStep1;
	}
}
