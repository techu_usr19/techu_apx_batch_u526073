package com.bbva.qwai.batch;

import java.util.HashMap;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Test for batch process QWAIJ001-01-US
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:/META-INF/spring/batch/beans/QWAIJ001-01-US-beans.xml","classpath:/META-INF/spring/batch/jobs/jobs-QWAIJ001-01-US-context.xml","classpath:/META-INF/spring/jobs-QWAIJ001-01-US-runner-context.xml"})
public class QWAIJ00101USFunctionalTest{

	@Autowired
	private JobLauncherTestUtils jobLauncherTestUtils;


	@Test
	public void testLaunchJob() throws Exception {
		//TODO implements job launch test
		//Without parameters (use this implementation if job not need parameters)
		//final JobExecution jobExecution = jobLauncherTestUtils.launchJob();
		
		//With parameters (use this implementation if job needs parameters comment first implementation) 
		/*********************** Parameters Definition ***********************/
		//Add parameters to job
		final HashMap<String, JobParameter> parameters = new HashMap<String, JobParameter>();
		parameters.put("itemsFile", new JobParameter("/tmp/helloworld.txt"));
		parameters.put("outputFile", new JobParameter("/tmp/helloworld_out.txt"));
		final JobParameters jobParameters = new JobParameters(parameters);
		final JobExecution jobExecution = jobLauncherTestUtils.launchJob(jobParameters);
		
		//TODO implements job launch test Assert's
		Assert.assertTrue(jobExecution.getExitStatus().equals(ExitStatus.COMPLETED));
	}
}
